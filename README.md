Kubernetes deployer image
=========================

Alpine based image with _bash_, _kubectl_, _Helm_ and some CLI tools.


Installed tools
---------------

- bash
- curl
- gettext
- helm
- kubectl
- wget


Automatic build
---------------

The image is built automatically and transparently with [Gitlab CI
pipeline][pipeline], pushed to [Docker Hub registry][registry] and can be
pulled using command

    docker pull randolf9/kube-deploy

or if you'd prefer to build it yourself from the source repository, clone this
repository locally and run:

    docker build -t randolf9/kube-deploy .

[pipeline]: https://gitlab.com/randolf.lemmont/kube-deploy-docker/-/pipelines
[registry]: https://hub.docker.com/r/randolf9/kube-deploy
